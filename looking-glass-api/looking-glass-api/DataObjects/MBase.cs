﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace looking_glass_api.DataObjects
{

    public class MBase
    {
        private string p_name;
        public string Name { get { return p_name; } set { p_name = value; } }

        private LookingGlassType parameterType;
        internal LookingGlassType ParameterType { get { return parameterType; } set { parameterType = value; } }

        public virtual object ReturnValue()
        {
            return null;
        }

    }
}
