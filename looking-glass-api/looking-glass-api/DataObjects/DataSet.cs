﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Runtime.CompilerServices;
using System.Globalization;
using System.Security.Cryptography;
[assembly: InternalsVisibleTo("LookingGlass")]

namespace looking_glass_api.DataObjects
{
    /// <summary>
    ///  convention: Parameter names must be unique. There is no support for duplication of parameter names through the API.
    /// </summary>
    public partial class DataSet : IDataSet
    {
        private const string NAME_LENGTH_ERROR = "Name of parameter must be under 255 characters in length";
        private string parentGlobalId;
        private string selfGlobalId;
        private List<Meta<int>> metaInt = new List<Meta<int>>();
        private List<Meta<double>> metaDouble = new List<Meta<double>>();
        private List<Meta<string>> metaString = new List<Meta<string>>();
        private List<Meta<DateTime>> metaDateTime = new List<Meta<DateTime>>();
        internal string ParentGlobalId { get { return parentGlobalId; } set { parentGlobalId = value; } }
        internal string SelfGlobalId { get { return selfGlobalId; } set { selfGlobalId = value; } }
        internal List<Meta<int>> MetaInt { get { return metaInt; } set { metaInt = value; } }
        internal List<Meta<double>> MetaDouble { get { return metaDouble; } set { metaDouble = value; } }
        internal List<Meta<string>> MetaString { get { return metaString; } set { metaString = value; } }
        internal List<Meta<DateTime>> MetaDateTime { get { return metaDateTime; } set { metaDateTime = value; } }
        private List<ParameterCollection<int>> parameterCollectionInt = new List<ParameterCollection<int>>();
        private List<ParameterCollection<double>> parameterCollectionDouble = new List<ParameterCollection<double>>();
        private List<ParameterCollection<string>> parameterCollectionString = new List<ParameterCollection<string>>();
        private List<ParameterCollection<DateTime>> parameterCollectionDate = new List<ParameterCollection<DateTime>>();
        internal List<ParameterCollection<int>> ParameterCollectionInt { get { return parameterCollectionInt; } set { parameterCollectionInt = value; } }
        internal List<ParameterCollection<double>> ParameterCollectionDouble { get { return parameterCollectionDouble; } set { parameterCollectionDouble = value; } }
        internal List<ParameterCollection<string>> ParameterCollectionString { get { return parameterCollectionString; } set { parameterCollectionString = value; } }
        internal List<ParameterCollection<DateTime>> ParameterCollectionDateTime { get { return parameterCollectionDate; } set { parameterCollectionDate = value; } }
        private List<string> exceptionList = new List<string>();
        static string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        Random ran = new Random();
        private bool firstWrite;
        private bool finalWrite;
        private bool isStartStream;
        private bool isParent;
        private bool isStreamType;
        private bool streamCalled;
        private bool firstStream;
        internal int StreamIndex;
        internal string project { get; set; }
        internal string task { get; set; }
        internal string source { get; set; }
        internal string user { get; set; }
        //internal string global_id { get; set; }
        internal int itemCount = 0;
        internal int ItemCount { get { return itemCount; } }
        internal string Name { get { return name; } set { name = value; } }
        private string name = "Unnamed_API_dataset";
        private DateTime Date { get { return date; } set { date = value; } }
        private DateTime date;
        private string folderPath = "";
        internal string FolderPath { get { return folderPath; } set { folderPath = value; } }
        private string filePath = "";
        internal string FilePath { get { return filePath; } set { filePath = value; } }

        bool CSVWhileStream { get { return cSVWhileStream; } set { cSVWhileStream = value; } }
        bool cSVWhileStream = false;
        internal string TempPath { get { return tempPath; } set { tempPath = value; } }
        private string tempPath = "";

        private string metaPath = "";
        internal string MetaPath { get { return metaPath; } set { metaPath = value; } }
        internal int uid { get; set; }
        private HashSet<string> parameterNames = new HashSet<string>();
        private List<PCBase> runningList = new List<PCBase>();
        byte[] buff = new byte[200];


        /// <summary>
        /// A dataset can be used to store your data in your local looking glass datastore. Use the AddParameter and EndItem calls to add data to your data set.
        /// </summary>
        public DataSet()
        {
            Name = "unnamed_API_dataset";
            Date = DateTime.Now;
            selfGlobalId = "API" + "_" + new string(Enumerable.Repeat(chars, 6).Select(s => s[ran.Next(s.Length)]).ToArray()) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            project = "local";
            task = "API_import";
            user = "API_local";
            source = "API_capture";
            firstWrite = true;
        }

        /// <summary>
        ///  A dataset can be used to store your data in your local looking glass datastore. Use the AddParameter and EndItem calls to add data to your data set.
        ///  This version of the DataSet constructor creates a DataSet configured for streaming. Calling the Stream function on this DataSet will send all the data captured up to that point to the local datastore.
        /// </summary>
        /// <param name="IsStream"></param>
        public DataSet(bool IsStream)
        {
            Name = "unnamed_API_dataset";
            Date = DateTime.Now;
            selfGlobalId = "API" + "_" + new string(Enumerable.Repeat(chars, 6).Select(s => s[ran.Next(s.Length)]).ToArray()) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            project = "local";
            task = "API_import";
            user = "API_local";
            source = "API_capture";
            if (IsStream)
            {
                isParent = true;
                isStartStream = true;
                parentGlobalId = selfGlobalId;
                isStreamType = true;
                StreamIndex = 0;
            }
            firstWrite = true;
        }

        /// <summary>
        /// Enable the writing of a complete CSV file with all data so far even if streaming is in use.
        /// Must be called before adding data to the dataset.
        /// </summary>
        public void EnableCSV(){
            if (firstWrite) { CSVWhileStream = true; }
        }

        internal void PaddedToIndexedAll()
        {
            allParams.ForEach(p => p.PaddedToIndexed());
        }
        internal void IndexToPaddedAllCollections()
        {
            int max = maxIndexedParamCollectionValue;
            allParams.ForEach(p => p.IndexedToPadded(max));
        }

        internal List<PCBase> allParams
        {
            get
            {
                List<PCBase> paramList = new List<PCBase>();
                ParameterCollectionDateTime.ForEach(pc => paramList.Add(pc));
                ParameterCollectionString.ForEach(pc => paramList.Add(pc));
                ParameterCollectionInt.ForEach(pc => paramList.Add(pc));
                ParameterCollectionDouble.ForEach(pc => paramList.Add(pc));
                return paramList;
            }
        }


        /// <summary>
        /// Use of this function not supported for datasets that have had items added using AddItem
        /// </summary>
        /// <param name="pc"></param>
        internal void AttachParameterCollection(PCBase pc)
        {
            if (!parameterNames.Contains(pc.Name))
            {
                if (pc.ParameterType == LookingGlassType.double_precision)
                {
                    ParameterCollectionDouble.Add((ParameterCollection<double>)pc);
                }
                else if (pc.ParameterType == LookingGlassType.integer)
                {
                    ParameterCollectionInt.Add((ParameterCollection<int>)pc);
                }
                else if (pc.ParameterType == LookingGlassType.string_value)
                {
                    ParameterCollectionString.Add((ParameterCollection<string>)pc);
                }
                else if (pc.ParameterType == LookingGlassType.date_time)
                {
                    ParameterCollectionDateTime.Add((ParameterCollection<DateTime>)pc);
                }
                parameterNames.Add(pc.Name);
            }

        }

        internal List<MBase> allMeta
        {
            get
            {
                List<MBase> mList = new List<MBase>();
                MetaDateTime.ForEach(pc => mList.Add(pc));
                MetaString.ForEach(pc => mList.Add(pc));
                MetaInt.ForEach(pc => mList.Add(pc));
                MetaDouble.ForEach(pc => mList.Add(pc));
                return mList;
            }
        }

        internal int maxParamCollectionCount
        {
            get
            {
                if (allParams.Count > 0)
                {
                    return allParams.Select(s => s.Count).Max();
                }
                else
                {
                    return 0;
                }
            }
        }

        internal int maxIndexedParamCollectionValue
        {
            get
            {
                return allParams.Select(s => s.MaxIndex).Max();
            }
        }

        public void AttachMeta(string name, int value) {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }
            string nameOrig = name;
            int repcount = 1;
            while (MetaInt.Select(a => a.Name).Contains(name))
            {
                name = nameOrig + "_" + repcount; 
                repcount++;
            }
            MetaInt.Add(new Meta<int>() { Name = name, Value = value, ParameterType = LookingGlassType.integer });
        }
        public void AttachMeta(string name, double value)
        {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }
            string nameOrig = name;
            int repcount = 1;
            while (MetaDouble.Select(a => a.Name).Contains(name))
            {
                name = nameOrig + "_" + repcount;
                repcount++;
            }
            MetaDouble.Add(new Meta<double>() { Name = name, Value = value, ParameterType = LookingGlassType.double_precision });
        }

        public void AttachMeta(string name, string value)
        {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }

            string nameOrig = name;
            int repcount = 1;
            while (MetaString.Select(a => a.Name).Contains(name))
            {
                name = nameOrig + "_" + repcount;
                repcount++;
            }
            MetaString.Add(new Meta<string>() { Name = name, Value = value, ParameterType = LookingGlassType.string_value });
        }
        public void AttachMeta(string name, DateTime value)
        {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }

            string nameOrig = name;
            int repcount = 1;

            while (MetaDateTime.Select(a => a.Name).Contains(name))
            {
                name = nameOrig + "_" + repcount;
                repcount++;
            }

            MetaDateTime.Add(new Meta<DateTime>() { Name = name, Value = value, ParameterType = LookingGlassType.date_time });
        }

        /// <summary>
        /// Set the datasets name. If not set the dataset name will be "Not defined".
        /// </summary>
        /// <param name="Name"></param>
        public void SetName(string Name)
        {


            if (Name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            } 
            this.Name = Name;
        }

        /// <summary>
        /// Set the datasets parent. Part of stream.
        /// </summary>
        /// <param name="globalId"></param>
        public void SetParent(string globalId)
        {
            this.ParentGlobalId = globalId;
        }

        /// <summary>
        /// Set the datasets task tag
        /// </summary>
        /// <param name="taskName"></param>
        public void SetTask(string taskName)
        {


            if (taskName.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            } 
            this.task = taskName;
        }

        /// <summary>
        /// Sets the folder path where the datasets data will be written on calls to EndWrite or Stream. If this is not set, the files will be written by default to "%ProgramData%\Mohago\lgdf"
        /// You can add these folder location to the API scan locations list in the Looking Glass client. The LG client will then periodically scan these locations for new data.
        /// </summary>
        /// <param name="FolderPath"></param>
        public void SetFolderPath(string FolderPath)
        {
            folderPath = FolderPath;
        }
        

        /// <summary>
        /// Set the datasets Datestamp. If not set a timestamp will be added when EndWrite is called.
        /// </summary>
        /// <param name="Date"></param>
        public void SetDate(DateTime Date)
        {
            date = Date;
        }

        private string createFileName()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            sb.Append("_");
            sb.Append(chars[ran.Next(25)]);
            sb.Append(new string(Enumerable.Repeat(chars, 5).Select(s => s[ran.Next(s.Length)]).ToArray()));
            sb.Append(".lgdf");
            return (sb.ToString());
        }

        internal void ClearCollections()
        {
            runningList.ForEach(a => a.ClearValues());
            itemCount = 0;
        }

        private String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        private string GenerateMetaString(looking_glass_api.DataObjects.DataSet ds, string metaPath)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            //settings.Encoding = Encoding.UTF32;
            //settings.
            var sw = new StringWriter();
            XmlWriter writer = XmlWriter.Create(sw, settings);
            writer.WriteProcessingInstruction("xml", "version='1.0'");
            XElement xe = new XElement((XName)"Meta");

            //writer.WriteStartDocument();
            writer.WriteStartElement("Meta");

            var allParams = ds.allParams.OrderBy(o => o.ParameterIndex).ToList();
            if (allParams.Count > 0)
            {
                writer.WriteStartElement("ParameterCollections");
                allParams.ForEach(pc =>
                {
                    writer.WriteStartElement("ParamCol");
                    writer.WriteAttributeString("Name", pc.Name);
                    writer.WriteAttributeString("Type", Enum.GetName(typeof(looking_glass_api.DataObjects.LookingGlassType), pc.ParameterType));
                    writer.WriteAttributeString("Index", Convert.ToString(pc.ParameterIndex));
                    writer.WriteEndElement();

                });
                writer.WriteEndElement();
            }

            var allMeta = ds.allMeta;
            if (allParams.Count > 0)
            {
                allMeta.ForEach(m =>
                {
                    string type = Enum.GetName(typeof(looking_glass_api.DataObjects.LookingGlassType), m.ParameterType);

                    writer.WriteStartElement(type);
                    writer.WriteAttributeString("Name", m.Name);

                    if (type == "date_time")
                    {
                        DateTime dt = (DateTime)m.ReturnValue();
                        //writer.WriteAttributeString("Value", Convert.ToString(m.ReturnValue(),CultureInfo.GetCultureInfo("en-gb")));

                        writer.WriteAttributeString("Value", ((DateTime)m.ReturnValue()).ToString("yyyyMMddHHmmssffff"));
                    }
                    else
                    {
                        writer.WriteAttributeString("Value", Convert.ToString(m.ReturnValue()));
                    }

                    writer.WriteEndElement();
                });

                if (ds.name != null)
                {
                    writer.WriteStartElement("DataSetName");
                    writer.WriteAttributeString("Value", ds.name);
                    writer.WriteEndElement();
                }

                if (ds.date != null)
                {
                    writer.WriteStartElement("DataSetDate");
                    writer.WriteAttributeString("Value", ds.date.ToString("yyyyMMddHHmmssffff"));
                    writer.WriteEndElement();
                }

                if (ds.project != null)
                {
                    writer.WriteStartElement("DataSetProject");
                    writer.WriteAttributeString("Value", ds.project);
                    writer.WriteEndElement();
                }

                if (ds.task != null)
                {
                    writer.WriteStartElement("DataSetTask");
                    writer.WriteAttributeString("Value", ds.task);
                    writer.WriteEndElement();
                }


                if (ds.source != null)
                {
                    writer.WriteStartElement("DataSetSource");
                    writer.WriteAttributeString("Value", ds.source);
                    writer.WriteEndElement();
                }

                if (!String.IsNullOrEmpty(ds.SelfGlobalId))
                {
                    writer.WriteStartElement("DataSetGlobalId");
                    writer.WriteAttributeString("Value", ds.SelfGlobalId);
                    writer.WriteEndElement();
                }
                if (isStartStream)
                {
                    writer.WriteStartElement("DataSetStartStream");
                    writer.WriteAttributeString("Value", "true");
                    writer.WriteEndElement();

                    

                }
                if (!isStartStream && !String.IsNullOrEmpty(ds.ParentGlobalId))
                {
                    writer.WriteStartElement("DataSetParent");
                    writer.WriteAttributeString("Value", ds.ParentGlobalId);
                    writer.WriteEndElement();
                }

                if (ds.isStreamType)
                {
                    writer.WriteStartElement("StreamIndex");
                    writer.WriteAttributeString("Value", ds.StreamIndex.ToString());
                    writer.WriteEndElement();
                }
                writer.WriteStartElement("Done");

                writer.WriteValue(finalWrite ? 1 : 0);
                writer.WriteEndElement();

                writer.WriteStartElement("Hash");
                // Read the file as one string.
                /*StringBuilder sb = new StringBuilder();
                using (StreamReader sr = new StreamReader(metaPath.Replace(".lgmf",".lgdf")))
                {
                    String line;
                    // Read and display lines from the file until the end of 
                    // the file is reached.
                    while ((line = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(line);
                    }
                }
                string allines = sb.ToString();
                string hash = CalculateMD5Hash(allines);*/

                //byte[] hash;
                //using (StreamReader sr = new StreamReader(metaPath.Replace(".lgmf", ".lgdf")))
                //{
                //    MD5CryptoServiceProvider m5 = new MD5CryptoServiceProvider();
                //    hash = m5.ComputeHash(sr.BaseStream);
                //    sr.Close();
                //}
                //// step 2, convert byte array to hex string
                //StringBuilder sb = new StringBuilder();
                //for (int i = 0; i < hash.Length; i++)
                //{
                //    sb.Append(hash[i].ToString("X2"));
                //}
                //writer.WriteValue(sb.ToString());
                string hash = ComputeHash(metaPath.Replace(".lgmf", ".lgdf"));
                writer.WriteValue(hash);
                writer.WriteEndElement();

                writer.WriteStartElement("Imported");
                writer.WriteValue(0);
                writer.WriteEndElement();
            }

            writer.WriteEndDocument();
            writer.Close();

            return sw.ToString();
        }


        private int parameterIndex = 0;
        
        private void SetElement(string path, string elementName, object value)
        {
            XDocument doc = XDocument.Load(path);
            XElement root = doc.Element(elementName);
            root.SetValue(value);
            doc.Save(path);
        }

        private string ComputeHash(string file)
        {
            byte[] buff2 = new byte[200];
            using (var md5 = MD5.Create())
            using (var stream = File.OpenRead(file))
            {
                buff2 = md5.ComputeHash(stream);
            }
            return BitConverter.ToString(buff2).Replace("-", "");
        }

        /// <summary>
        /// needs whole file cause memory overrun
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// PeriodicWrite and Close, replace the Write(string filePath) function
        /// Now every N items the dataset data is written to a file.
        /// At the end of the capture operation Close is called. This writes the remaining N-X items to the file and appends the XML structure + dataset meta data string.
        /// Call this from item add every N items.
        /// </summary>
        /// <returns></returns>
        internal void PeriodicWrite()
        {

            //Need to make sure everything happens ok here.
            //You should be able to periodically write to the lgdf file, and then create a new lgf pair if stream is called

           // if (firstWrite)
            if (firstWrite)
            {
                CreateNewLgf();
                TempPath = System.IO.Path.ChangeExtension(FilePath, "lgtf");
                firstWrite = false;
            }

            //fileStream = new FileStream(FilePath, FileMode.Create);
            //txtWriter = new StreamWriter(fileStream);

            // }
            if (!CSVWhileStream)
            {
                WriteToLgdf();
            }
            else
            {
                WriteToLgdfLgtf();
            }
           
            ///clear the parameter collections.
            ClearCollections();
            //increment after write

        }

        private void WriteToLgdf()
        {
            using (FileStream fileIn = new FileStream(FilePath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fileIn))
                {
                    ///write the values in all parameter collection to the file path.
                    var ds = this;
                    var pars = runningList;
                    pars = pars.OrderBy(o => o.ParameterIndex).ToList();
                    int paramCount = pars.Count;
                    int itemCount = ds.maxParamCollectionCount;

                    string s;
                    for (int i = 0; i < itemCount; i++)
                    {
                        s = "";
                        for (int paramCol = 0; paramCol < paramCount; paramCol++)
                        {
                            s += pars[paramCol].StringValueAt(i);
                            s += ",";
                        }
                        s = s.Remove(s.Length - 1, 1);
                        s += "\r\n";
                        sw.Write(s);
                    }
                }
            }
        }

        private void WriteToLgdfLgtf()
        {
            using (FileStream fileIn = new FileStream(FilePath, FileMode.Append, FileAccess.Write), fileIn2 = new FileStream(TempPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fileIn), sw2 = new StreamWriter(fileIn2))
                {
                    ///write the values in all parameter collection to the file path.
                    var ds = this;
                    var pars = runningList;
                    pars = pars.OrderBy(o => o.ParameterIndex).ToList();
                    int paramCount = pars.Count;
                    int itemCount = ds.maxParamCollectionCount;

                    string s;
                    for (int i = 0; i < itemCount; i++)
                    {
                        s = "";
                        for (int paramCol = 0; paramCol < paramCount; paramCol++)
                        {
                            s += pars[paramCol].StringValueAt(i);
                            s += ",";
                        }
                        s = s.Remove(s.Length - 1, 1);
                        s += "\r\n";
                        sw.Write(s); sw2.Write(s);
                    }
                }
            }
        }

        private string CreateNewLgf()
        {
            bool folderExists = System.IO.Directory.Exists(FolderPath);
            if (!folderExists)
            {
                //string programDataPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData, Environment.SpecialFolderOption.None);

                string programDataPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                FolderPath = programDataPath + "\\Mohago\\Lgdf";
                if (!System.IO.Directory.Exists(FolderPath))
                {
                    System.IO.Directory.CreateDirectory(FolderPath);
                }
            }
             return FilePath = FolderPath + "\\" + createFileName();
        }



        /// <summary>
        /// Calling EndWrite closes the current dataset. All captured data will be written to the default or specified file locations in the Looking Glass data file format.
        /// The local client will import these files and add them to the datastore.
        /// </summary>
        /// <returns></returns>
        public string EndWrite()
        {
            if (isStreamType)
            {
                finalWrite = true;
                if(!streamCalled){
                    throw new Exception("Stream() must be called at least once before calling EndWrite() on a Stream-type Dataset.");
                }
                //firstWrite = true;
            }       
            ///write any remaining data
            PeriodicWrite();
            
            //txtWriter.Close();
            //fileStream.Close();
            if (CSVWhileStream)
            {
                if (File.Exists(TempPath))
                {
                    File.Delete(TempPath);
                }
            }
            ///create a metafile
            WriteLGMF();
            //streamNewFile = true;//Needed?
            //firstWrite = true;
            return FilePath;
        }

        /// <summary>
        /// Calling Stream writes any data captured since the creation of the dataset, or since the last call to EndWrite or Stream, to a Looking Glass file.
        /// This data will then be imported to the datastore by the client.
        /// After calling Stream you can continue to add more data to the this dataset.
        /// Subsequent calls to Stream will update the dataset instance in the datastore with any new data.
        /// </summary>
        /// <returns></returns>
        public string Stream()
        {
            streamCalled = true;
            if (!isStreamType)
            {
                throw new Exception("Stream can only be called on Stream-type Datasets. Pass in a Boolean of value True to the Dataset constructor to create a Stream-type Dataset."); 
            }
            ///write any remaining data
            //firstWrite = true;

            //ADd method to tidy up existing lgdf
            

            PeriodicWrite();

            ///create a metafile
            MetaPath = System.IO.Path.ChangeExtension(FilePath, "lgmf");
            using (FileStream fileIn = new FileStream(MetaPath, FileMode.Append, FileAccess.Write))
            {
                //txtWriter = new StreamWriter(fileStream);
                using (StreamWriter sw = new StreamWriter(fileIn))
                {
                    string s = "";
                    s = GenerateMetaString(this, MetaPath);
                    if (s != null && s != "") { sw.Write((s)); }
                }

            }

            CreateNewLgf();
            
            this.selfGlobalId = "API" + "_" + new string(Enumerable.Repeat(chars, 6).Select(s => s[ran.Next(s.Length)]).ToArray()) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            isStartStream = false;

            if (isStreamType)
            {
                StreamIndex++;
            }

            return FilePath;
        }

        private void WriteLGMF()
        {
            MetaPath = System.IO.Path.ChangeExtension(FilePath, "lgmf");
            using (FileStream fileIn = new FileStream(MetaPath, FileMode.Append, FileAccess.Write))
            {
                //txtWriter = new StreamWriter(fileStream);
                using (StreamWriter sw = new StreamWriter(fileIn))
                {
                    string s = "";
                    s = GenerateMetaString(this, MetaPath);
                    if (s != null && s != "") { sw.Write((s)); }
                }

            }
        }

        private string GenerateParamString(looking_glass_api.DataObjects.DataSet ds)
        {
            string output = "";
            var allParams = ds.allParams.OrderBy(o => o.ParameterIndex).ToList();
            if (allParams.Count > 0)
            {
                allParams.ForEach(pc =>
                {
                    if (allParams.Last() != pc)
                    {
                        output += (pc.Name + ",");
                    }
                    else
                    {
                        output += (pc.Name);
                    }
                });
            }
            return output;
        }
        /// <summary>
        /// Writes the current Dataset to a .csv file
        /// </summary>
        /// <param name="path"></param>
        public void WriteCSV(string path)
        {
            if (CSVWhileStream)
            {
                PeriodicWrite();

                using (FileStream fileOut = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    //txtWriter = new StreamWriter(fileStream);
                    using (StreamWriter sw = new StreamWriter(fileOut))
                    {
                        string s = "";
                        s = GenerateParamString(this);
                        if (s != null && s != "") { sw.WriteLine((s)); } ///write the top line.

                        using (FileStream fileIn = new FileStream(TempPath, FileMode.Open, FileAccess.Read))
                        {
                            StreamReader sr = new StreamReader(fileIn);
                            string lineData;
                            while ((lineData = sr.ReadLine()) != null)
                            {
                                sw.WriteLine(lineData);
                            }
                            fileIn.Close();
                        }

                        sw.Close();
                        fileOut.Close();
                    }

                }
            }
        }

        /// <summary>
        /// This will add a new parameter of type double called "name" and value "value" to the current Item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddParameterDouble(string name,double value)
        {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }
            if (parameterNames.Contains(name))
            {

                if (parameterCollectionDouble.Any(pC => pC.Name == name))
                {
                    var pCNew = parameterCollectionDouble.Find(pC => pC.Name == name); 
                    if (!pCNew.NewItemThisRound)
                    {
                        pCNew.Values.Add(new Pair<bool, double>(true, value));
                        pCNew.NewItemThisRound = true;
                    }
                }
                else
                {
                    ///name type uniqueness exception
                }
            }
            else
            {
                var pCNew = new ParameterCollection<double>() { Name = name, ParameterType = looking_glass_api.DataObjects.LookingGlassType.double_precision, ParameterIndex = parameterIndex }; parameterIndex++;
                for (int i = 0; i < itemCount; i++) { pCNew.PadValue(); }
                parameterCollectionDouble.Add(pCNew); runningList.Add(pCNew);
                pCNew.Values.Add(new Pair<bool, double>(true, value)); pCNew.NewItemThisRound = true;
                parameterNames.Add(name);
            }
        }
        /// <summary>
        /// This will add a new parameter of type string called "name" and value "value" to the current Item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddParameterString(string name, string value)
        {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }
            if (parameterNames.Contains(name))
            {

                if (parameterCollectionString.Any(pC => pC.Name == name))
                {
                    var pCNew = parameterCollectionString.Find(pC => pC.Name == name);
                    if (!pCNew.NewItemThisRound)
                    {
                        pCNew.Values.Add(new Pair<bool, string>(true, value));
                        pCNew.NewItemThisRound = true;
                    }
                }
                else
                {
                    ///name type uniqueness exception
                }
            }
            else
            {
                var pCNew = new ParameterCollection<string>() { Name = name, ParameterType = looking_glass_api.DataObjects.LookingGlassType.string_value, ParameterIndex = parameterIndex }; parameterIndex++;
                for (int i = 0; i < itemCount; i++) { pCNew.PadValue(); }
                parameterCollectionString.Add(pCNew); runningList.Add(pCNew);
                pCNew.Values.Add(new Pair<bool, string>(true, value)); pCNew.NewItemThisRound = true;
                parameterNames.Add(name);
            }
        }

        /// <summary>
        /// This will add a new parameter of Integer type called "name" and value "value" to the current Item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddParameterInt(string name, int value)
        {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }
            if (parameterNames.Contains(name))
            {

                if (parameterCollectionInt.Any(pC => pC.Name == name))
                {
                    var pCNew = parameterCollectionInt.Find(pC => pC.Name == name); 
                    if (!pCNew.NewItemThisRound)
                    {
                        pCNew.Values.Add(new Pair<bool, int>(true, value));
                        pCNew.NewItemThisRound = true;
                    }
                }
                else
                {
                    ///name type uniqueness exception
                }
            }
            else
            {
                var pCNew = new ParameterCollection<int>() { Name = name, ParameterType = looking_glass_api.DataObjects.LookingGlassType.integer, ParameterIndex = parameterIndex }; parameterIndex++;
                for (int i = 0; i < itemCount; i++) { pCNew.PadValue(); }
                parameterCollectionInt.Add(pCNew); runningList.Add(pCNew);
                pCNew.Values.Add(new Pair<bool, int>(true, value)); pCNew.NewItemThisRound = true;
                parameterNames.Add(name);
            }
        }

        /// <summary>
        /// This will add a new parameter of .NET compatible DateTime type called "name" and value "value" to the current Item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddParameterDate(string name, DateTime value)
        {

            if (name.Length > 255)
            {
                throw new Exception(NAME_LENGTH_ERROR);
            }
            if (parameterNames.Contains(name))
            {

                if (parameterCollectionDate.Any(pC => pC.Name == name))
                {
                    var pCNew = parameterCollectionDate.Find(pC => pC.Name == name); 
                    if (!pCNew.NewItemThisRound)
                    {
                        pCNew.Values.Add(new Pair<bool, DateTime>(true, value));
                        pCNew.NewItemThisRound = true;
                    }
                }
                else
                {
                    ///name type uniqueness exception
                }
            }
            else
            {
                var pCNew = new ParameterCollection<DateTime>() { Name = name, ParameterType = looking_glass_api.DataObjects.LookingGlassType.date_time, ParameterIndex = parameterIndex }; parameterIndex++;
                for (int i = 0; i < itemCount; i++) { pCNew.PadValue(); }
                parameterCollectionDate.Add(pCNew); runningList.Add(pCNew);
                pCNew.Values.Add(new Pair<bool, DateTime>(true, value)); pCNew.NewItemThisRound = true;
                parameterNames.Add(name);
            }
        }

        /// <summary>
        /// The ends the current Item. Subsequent calls to the AddParameter functions will be adding values to a new Item.
        /// </summary>
        public void EndItem()
        {
            runningList.ForEach(a =>
            {
                if (a.NewItemThisRound == false)
                {
                    a.PadValue();
                }
                a.NewItemThisRound = false;
            });
            if (itemCount > 100)
            {
                PeriodicWrite();
            }
            else
            {
                itemCount++;
            }
        } 
    }
}
