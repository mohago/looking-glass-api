﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace looking_glass_api.DataObjects
{
    public class Item : IItem
    {
        private List<Parameter<int>> parameterInt = new List<Parameter<int>>();
        private List<Parameter<double>> parameterDouble = new List<Parameter<double>>();
        private List<Parameter<string>> parameterString = new List<Parameter<string>>();
        private List<Parameter<DateTime>> parameterDateTime = new List<Parameter<DateTime>>();

        internal List<Parameter<int>> ParameterInt { get { return parameterInt; } set { parameterInt = value; } }
        internal List<Parameter<double>> ParameterDouble { get { return parameterDouble; } set { parameterDouble = value; } }
        internal List<Parameter<string>> ParameterString { get { return parameterString; } set { parameterString = value; } }
        internal List<Parameter<DateTime>> ParameterDateTime { get { return parameterDateTime; } set { parameterDateTime = value; } }
        internal int parameterIndex = 0;

        private HashSet<string> parameterNames = new HashSet<string>();
        public HashSet<string> ParameterNames { get { return parameterNames; } }
        
        private HashSet<LookingGlassType> parameterTypes = new HashSet<LookingGlassType>();
        internal HashSet<LookingGlassType> ParameterTypes { get { return parameterTypes; } }

        internal List<PBase> AllParameters
        {
            get
            {
                List<PBase> ret = new List<PBase>();
                parameterDouble.ForEach(p => ret.Add(p));
                parameterInt.ForEach(p => ret.Add(p));
                parameterString.ForEach(p => ret.Add(p));
                parameterDateTime.ForEach(p => ret.Add(p));
                return ret;
            }
        }

        #region add parameters

            ///No duplication of parameter names is allowed.

       

        public bool AddParameterDouble(string Name, double Value)
        {

            if (parameterNames.Any(pd => pd == Name))
            {
                return false;
            }
            else
            {
                ParameterDouble.Add(new Parameter<double>() { Name = Name, Value = Value, ParameterIndex = parameterIndex, ParameterType = LookingGlassType.double_precision });
                parameterIndex++; parameterNames.Add(Name); parameterTypes.Add(LookingGlassType.double_precision);
                return true;
            }
        }
        public bool AddParameterString(string Name, string Value)
        {
            if (parameterNames.Any(pd => pd == Name))
            {
                return false;
            }
            else
            {
                ParameterString.Add(new Parameter<string>() { Name = Name, Value = Value, ParameterIndex = parameterIndex, ParameterType = LookingGlassType.string_value });
                parameterIndex++; parameterNames.Add(Name); parameterTypes.Add(LookingGlassType.string_value);
                return true;
            }
        }
        public bool AddParameterDateTime(string Name, DateTime Value)
        {
            if (parameterNames.Any(pd => pd == Name))
            {
                return false;
            }
            else
            {
                ParameterDateTime.Add(new Parameter<DateTime>() { Name = Name, Value = Value, ParameterIndex = parameterIndex, ParameterType = LookingGlassType.date_time });
                parameterIndex++; parameterNames.Add(Name); parameterTypes.Add(LookingGlassType.date_time);
                return true;
            }
        }
        public bool AddParameterInt(string Name, int Value)
        {
            if (ParameterNames.Any(pd => pd == Name))
            {
                return false;
            }
            else
            {
                ParameterInt.Add(new Parameter<int>() { Name = Name, Value = Value, ParameterIndex = parameterIndex, ParameterType = LookingGlassType.integer });
                parameterIndex++; parameterNames.Add(Name); parameterTypes.Add(LookingGlassType.integer);
                return true;
            }
        }
        #endregion


        
    }
}
