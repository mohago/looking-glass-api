﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace looking_glass_api.DataObjects
{
    internal class Parameter<T>:PBase
    {
        private T p_value;
        public T Value { get { return p_value; } set { p_value = value; } }
    }
}
