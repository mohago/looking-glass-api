﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace looking_glass_api.DataObjects
{
    public enum LookingGlassType:byte
    {
        integer, double_precision, string_value, date_time, floating_point,unknown
    }
}
