﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace looking_glass_api.DataObjects
{
    interface IDataSet
    {
        
        void AttachMeta(string name, int value);
        void AttachMeta(string name, double value);
        void AttachMeta(string name, string value);
        void AttachMeta(string name, DateTime value);
        void AddParameterDouble(string name, double value);
        void AddParameterDate(string name, DateTime value);
        void AddParameterString(string name, string value);
        void AddParameterInt(string name, int value);
        void SetName(string Name);
        void SetDate(DateTime Date);
        void SetParent(string parentGlobalId);
        void SetFolderPath(string FolderPath);
        void SetTask(string Task);
        void EnableCSV();
        void EndItem();
        string EndWrite();
        string Stream();
        void WriteCSV(string filePath);
    }
}
