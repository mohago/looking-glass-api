﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace looking_glass_api.DataObjects
{
    interface IItem
    {
        
        bool AddParameterDouble(string Name, double value);
        bool AddParameterString(string Name, string value);
        bool AddParameterDateTime(string Name, DateTime value);
        bool AddParameterInt(string Name, int value);
    }
}
