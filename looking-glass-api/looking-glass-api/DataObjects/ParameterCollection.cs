﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
//using System.Threading.Tasks;

namespace looking_glass_api.DataObjects
{
        

    internal class  ParameterCollection<T>:PCBase
    {
        private List<Pair<bool,T>> p_values = new List<Pair<bool,T>>();
        public List<Pair<bool, T>> Values { get { return p_values; } set { p_values = value; } }

        private List<Pair<int, T>> p_indexedvalues = new List<Pair<int, T>>();
        public List<Pair<int, T>> IndexedValues { get { return p_indexedvalues; } set { p_indexedvalues= value; } }

       
        /// <summary>
        /// add value from external source with error checking on null, maybe extend to parse.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="newValue"></param>
        public override void AddValue(int index,object newValue)
        {

                var val = (T)Convert.ChangeType(newValue, typeof(T));

                if (ParameterType == LookingGlassType.double_precision)
                {
                    double r;
                    if (double.TryParse((string)newValue, out r))
                    {
                        IndexedValues.Add(new Pair<int, T>(index, (T)Convert.ChangeType(r, typeof(T))));
                    }
                }
                else if (ParameterType == LookingGlassType.integer)
                {
                    int r;
                    if (int.TryParse((string)newValue, out r))
                    {
                        IndexedValues.Add(new Pair<int, T>(index, (T)Convert.ChangeType(r, typeof(T))));
                    }
                }
                else if (ParameterType == LookingGlassType.date_time)
                {
                    DateTime r;
                    if (DateTime.TryParse((string)newValue, out r))
                    {
                        IndexedValues.Add(new Pair<int, T>(index, (T)Convert.ChangeType(r, typeof(T))));
                    }
                }
                else if (ParameterType == LookingGlassType.string_value)
                {
                    IndexedValues.Add(new Pair<int, T>(index, (T)Convert.ChangeType(newValue, typeof(T))));
                }
        }

        public override int Count
        {
            get
            {
                return Values.Count;
            }
        }

        public override int MaxIndex
        {
            get
            {
                if (IndexedValues != null && IndexedValues.Count > 0)
                {
                    return IndexedValues.Select(s => s.First).Max();
                }
                else
                {
                    return -1;
                }
            }
        }
        /// <summary>
        /// Return value at index. No error checking for speed, assumes all parameter collections are same length.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override object ValueAt(int index)
        {
            if(Values[index].First){
                return Values[index].Second;
            }else{
                return null;
            }
        }

//        internal static String TimeZoneName(DateTime dt)
//        {
//            String sName = TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt)
//                ? TimeZone.CurrentTimeZone.DaylightName
//                : TimeZone.CurrentTimeZone.StandardName;
//
//            String sNewName = "";
//            String[] sSplit = sName.Split(new char[] { ' ' });
//            foreach (String s in sSplit)
//                if (s.Length >= 1)
//                    sNewName += s.Substring(0, 1);
//
//            return sNewName;
//        }

        internal override string StringValueAt(int index)
        {
            string ret = "";
            
            if (Values[index].First&&Values[index].Second!=null)
            {

                if (ParameterType.Equals(LookingGlassType.date_time))
                {
                    DateTime dt = (DateTime)(object)Values[index].Second;
                    //DateTime dt = DateTime.Parse(Convert.ToString(Values[index].Second));
                    ret = dt.ToString("yyyyMMddHHmmssffff");
                }
                else
                {
                    ret = Convert.ToString(Values[index].Second);
                }
                
            }

            return ret;
        }
        public override void PadValue()
        {
           Values.Add(new Pair<bool,T>(false,default(T)));
        }

        public override void ClearValues()
        {
            Values.Clear();
        }
        

        #region write/read helpers
        public override void PaddedToIndexed()
        {
            int i=0;
            IndexedValues.Clear();
            foreach (var v in Values)
            {
                if (v.First) { IndexedValues.Add(new Pair<int, T>(i, v.Second)); }
                i++;
            }
        }
        public override void IndexedToPadded(int padTo)
        {
            Values.Clear();
            
            int i = 0;
            foreach (var v in IndexedValues)
            {
                var index = v.First;
                while (i < index)
                {
                    Values.Add(new Pair<bool, T>(false, default(T)));
                    i++;
                }
                Values.Add(new Pair<bool, T>(true, v.Second));
                i++;
            }
            
            while (i <= padTo)
            {
                Values.Add(new Pair<bool, T>(false, default(T)));
                i++;
            }

        }
        #endregion
    }


  
}
