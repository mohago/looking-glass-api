﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("LookingGlass")]

namespace looking_glass_api.DataObjects
{
    internal class PCBase:PBase
    {

        public virtual void ClearValues()
        {

        }

        public virtual int Count { get { return -1; } }

        public virtual int MaxIndex { get { return -1; } }

        public virtual object ValueAt(int index)
        {
            return null;
        }
        internal virtual string StringValueAt(int index)
        {
            return "";
        }

        public virtual void PadValue() { }
        public virtual void AddValue(int index, object newValue)
        {

        }

        private bool newItemThisRound = false;
        public bool NewItemThisRound { get { return newItemThisRound; } set { newItemThisRound = value; } }

        /// <summary>
        /// converts from IndexedValues to padded Values
        /// </summary>
        public virtual void IndexedToPadded(int padTo)
        {

        }

        public virtual void PaddedToIndexed()
        {

        }
    }
}
