﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("LookingGlass")]

namespace looking_glass_api.DataObjects
{
    internal class PBase
    {
        private string p_name;
        public string Name { get { return p_name; } set { p_name = value; } }

        private LookingGlassType parameterType;
        public LookingGlassType ParameterType { get { return parameterType; } set { parameterType = value; } }

        private int parameterIndex = -1;
        public int ParameterIndex { get { return parameterIndex; } set { parameterIndex = value; } }
    }
}
